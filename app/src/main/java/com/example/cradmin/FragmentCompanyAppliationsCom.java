package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.Adapters.CompanyApplicationsRecyclerAdapter;
import com.example.cradmin.ModelClass.CompanyApplications;
import com.example.cradmin.ModelClass.CompanyApplicationsDetails;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentCompanyAppliationsCom extends Fragment implements CompanyApplicationsRecyclerAdapter.Adapterclick{
    ProgressDialog progressDialog;
    ArrayList<CompanyApplicationsDetails> Details_array=new ArrayList<>();
    RecyclerView recyclerView;
    CompanyApplicationsRecyclerAdapter companyApplicationsRecyclerAdapter;

    public FragmentCompanyAppliationsCom() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_company_appliations_com, container, false);

        recyclerView=root.findViewById(R.id.company_applications_recyclerview_widget);
        set_data();

        companyApplicationsRecyclerAdapter=new CompanyApplicationsRecyclerAdapter(FragmentCompanyAppliationsCom.this,Details_array);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(companyApplicationsRecyclerAdapter);
        companyApplicationsRecyclerAdapter.notifyDataSetChanged();

        return root;
    }
    public void set_data()
    {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service=retrofit.create(ApiService.class);
        Call<CompanyApplications> call=service.getjoblists("hjvj");

        call.enqueue(new Callback<CompanyApplications>() {
            @Override
            public void onResponse(Call<CompanyApplications> call, Response<CompanyApplications> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().isSuccess())
                    {
                        Details_array.clear();
                        Details_array.addAll(response.body().getRequests_details());
                        companyApplicationsRecyclerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CompanyApplications> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void recyclerviewclick(int position) {

        Intent intent=new Intent(getContext(),CompanyApplicationsDetailsActivity.class);
        intent.putExtra("cr_id",Details_array.get(position).getCr_id());
        intent.putExtra("companyname",Details_array.get(position).getCr_company_name());
        intent.putExtra("language",Details_array.get(position).getCr_language());
        intent.putExtra("interview_date",Details_array.get(position).getCr_interview_date());
        intent.putExtra("email",Details_array.get(position).getCr_email());
        intent.putExtra("mobile",Details_array.get(position).getCr_mobile());
        intent.putExtra("interview_place",Details_array.get(position).getCr_interview_place());
        intent.putExtra("website",Details_array.get(position).getCr_website());
        intent.putExtra("otherdetails",Details_array.get(position).getCr_otherdetails());
        startActivity(intent);
    }

}
