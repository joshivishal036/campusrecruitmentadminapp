package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.Adapters.NewCompaniesRecyclerAdapter;
import com.example.cradmin.ModelClass.Companies;
import com.example.cradmin.ModelClass.DetailsCompany;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentNewCompanies extends Fragment implements NewCompaniesRecyclerAdapter.Adapterclick {
    ArrayList<DetailsCompany> companies=new ArrayList<>();
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;
    NewCompaniesRecyclerAdapter newCompaniesRecyclerAdapter;

    public FragmentNewCompanies() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View root=inflater.inflate(R.layout.fragment_new_companies, container, false);

        recyclerView=root.findViewById(R.id.new_companies_recyclerview);

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service=retrofit.create(ApiService.class);

        Call<Companies> call=service.getcompanies("jayshreeram");

        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().isSuccess())
                    {
                        companies.clear();
                        companies.addAll(response.body().getNew_company_details());
                        newCompaniesRecyclerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

        newCompaniesRecyclerAdapter =new NewCompaniesRecyclerAdapter(FragmentNewCompanies.this,companies);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(newCompaniesRecyclerAdapter);
        newCompaniesRecyclerAdapter.notifyDataSetChanged();

       return root;
    }

    @Override
    public void recyclerviewclick(int position) {
        //Toast.makeText(this, "position is"+position, Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(getContext(),CompaniesDetailsActivity.class);
        intent.putExtra("profileimage", companies.get(position).getCompany_profileimage_url());
        intent.putExtra("comID",companies.get(position).getCompany_id());
        intent.putExtra("name", companies.get(position).getCompany_name());
        intent.putExtra("email", companies.get(position).getCompany_email());
        intent.putExtra("mobile", companies.get(position).getCompany_mobile());
        intent.putExtra("website", companies.get(position).getCompany_website());
        intent.putExtra("address", companies.get(position).getCompany_address());
        intent.putExtra("state", companies.get(position).getCompany_state());
        intent.putExtra("city", companies.get(position).getCompany_city());
        intent.putExtra("status",companies.get(position).getCompany_status());
        Log.d("joshivishal", "isss>>> "+companies.get(position).getCompany_name()+companies.get(position).getCompany_email()+companies.get(position).getCompany_mobile()+ companies.get(position).getCompany_website()+ companies.get(position).getCompany_address()+companies.get(position).getCompany_state()+companies.get(position).getCompany_city());
        startActivity(intent);

    }

}
