package com.example.cradmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class StudentApplicationDetailsActivity extends AppCompatActivity {
    TextView companyname_tv,language_tv,interdate_tv,cemail_tv,cmobile_tv,interplace_tv,website_tv,otherdetails_tv,studname_tv,gender_tv,semail_tv,
            smobile_tv,collegename_tv,branch_tv,sem_tv,enroll_num_tv,cgpa_tv,applieddate_tv,photoupload_tv,resumeupload_tv;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_applications_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyname_tv=findViewById(R.id.application_details_company_name_tv);
        language_tv=findViewById(R.id.application_details_language_tv);
        interdate_tv=findViewById(R.id.application_details_interview_date_tv);
        cemail_tv=findViewById(R.id.application_details_email_tv);
        cmobile_tv=findViewById(R.id.application_details_mobile_tv);
        interplace_tv=findViewById(R.id.application_details_place_Interview_tv);
        website_tv=findViewById(R.id.application_details_website_tv);
        otherdetails_tv=findViewById(R.id.application_details_other_details_tv);
        studname_tv=findViewById(R.id.application_details_stud_name_tv);
        gender_tv=findViewById(R.id.application_details_gender_tv);
        semail_tv=findViewById(R.id.application_details_stud_email_tv);
        smobile_tv=findViewById(R.id.application_details_stud_mobile_tv);
        collegename_tv=findViewById(R.id.application_details_college_tv);
        branch_tv=findViewById(R.id.application_details_branch_tv);
        sem_tv=findViewById(R.id.application_details_sem_tv);
        enroll_num_tv=findViewById(R.id.application_details_enrollment_tv);
        cgpa_tv=findViewById(R.id.application_details_cgpa_tv);
        applieddate_tv=findViewById(R.id.application_details_applied_date_tv);
        photoupload_tv=findViewById(R.id.application_details_photo_tv);
        resumeupload_tv=findViewById(R.id.application_details_resume_tv);

//        Toast.makeText(this,getIntent().getStringExtra("stud_app_com_mobile") , Toast.LENGTH_SHORT).show();

        companyname_tv.setText(getIntent().getStringExtra("stud_app_com_name"));
        language_tv.setText(getIntent().getStringExtra("stud_app_com_language"));
        interdate_tv.setText(getIntent().getStringExtra("stud_app_com_interdate"));
        cemail_tv.setText(getIntent().getStringExtra("stud_app_com_email"));
        cmobile_tv.setText(getIntent().getStringExtra("stud_app_com_mobile"));
        interplace_tv.setText(getIntent().getStringExtra("stud_app_com_interplace"));
        website_tv.setText(getIntent().getStringExtra("stud_app_com_website"));
        otherdetails_tv.setText(getIntent().getStringExtra("stud_app_com_otherdetails"));
        studname_tv.setText(getIntent().getStringExtra("stud_app_stud_name"));
        gender_tv.setText(getIntent().getStringExtra("stud_app_stud_gender"));
        semail_tv.setText(getIntent().getStringExtra("stud_app_stud_email"));
        smobile_tv.setText(getIntent().getStringExtra("stud_app_stud_mobile"));
        collegename_tv.setText(getIntent().getStringExtra("stud_app_stud_clgname"));
        branch_tv.setText(getIntent().getStringExtra("stud_app_stud_branch"));
        sem_tv.setText(getIntent().getStringExtra("stud_app_stud_sem"));
        enroll_num_tv.setText(getIntent().getStringExtra("stud_app_stud_enrollment"));
        cgpa_tv.setText(getIntent().getStringExtra("stud_app_stud_cgpa"));
        applieddate_tv.setText(getIntent().getStringExtra("stud_app_stud_applieddate"));
        photoupload_tv.setText(getIntent().getStringExtra("stud_app_stud_photourl"));
        resumeupload_tv.setText(getIntent().getStringExtra("stud_app_stud_fileurl"));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
