package com.example.cradmin.APIs;


import com.example.cradmin.ModelClass.CollegeApplications;
import com.example.cradmin.ModelClass.Companies;
import com.example.cradmin.ModelClass.CompanyApplications;
import com.example.cradmin.ModelClass.Result;
import com.example.cradmin.ModelClass.Student;
import com.example.cradmin.ModelClass.StudentApplications;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @FormUrlEncoded
    @POST("login.php")
    Call<Result> AdminLogin(
            @Field("adminID") String adminID,
            @Field("password") String password,
            @Field("user_type") String user_type
    );
//
//    @FormUrlEncoded
//    @POST("state.php")
//    Call<State> getstate(
//            @Field("user_type") String user_type
//    );
//
//    @FormUrlEncoded
//    @POST("city.php")
//    Call<City> getcity(
//            @Field("S") String S
//    );
//
//    @FormUrlEncoded
//    @POST("branch.php")
//    Call<Branch> getbranch(
//            @Field("user_type") String usertype
//    );
//
//    @FormUrlEncoded
//    @POST("semester.php")
//    Call<Semester> getsemester(
//            @Field("user_type") String usertype
//    );
//
//
//
//    @POST("Registration.php")
//    Call<Result> studentRegistration(@Body RequestBody profile_image);
//
//
//    @FormUrlEncoded
//    @POST("getprofile.php")
//    Call<Student> studentdetails(
//            @Field("user_type") String user_type,
//            @Field("studID") String studID
//
//    );
//
//    @Multipart
//    @POST("updateprofile.php")
//    Call<Result> updateprofile(@Part("user_type") RequestBody user_type,
//                               @Part("studID") RequestBody studID,
//                               @Part("fname") RequestBody fname,
//                               @Part("gender") RequestBody gender,
//                               @Part("email") RequestBody email,
//                               @Part("mobile") RequestBody mobile,
//                               @Part("address") RequestBody address,
//                               @Part("state") RequestBody state,
//                               @Part("city") RequestBody city,
//                               @Part("college") RequestBody college,
//                               @Part("branch") RequestBody branch,
//                               @Part("sem") RequestBody sem,
//                               @Part("enroll") RequestBody enroll,
//                               @Part MultipartBody.Part profile_image
//    );
//
//
//    @FormUrlEncoded
//    @POST("changepassword.php")
//    Call<Result> changepassword(
//            @Field("user_type") String user_type,
//            @Field("studID") String studID,
//            @Field("oldpass") String oldpass,
//            @Field("newpass") String newpass
//
//    );
//
    @FormUrlEncoded
    @POST("getcompanies.php")
    Call<Companies> getcompanies(
            @Field("string") String string
    );

    @FormUrlEncoded
    @POST("getstudents.php")
    Call<Student> getstudents(
            @Field("string") String string
    );

    @FormUrlEncoded
    @POST("approve_delete_students.php")
    Call<Result> approvestudents(
            @Field("Action_type") String Action_type,
            @Field("ID") String ID
    );
    @FormUrlEncoded
    @POST("approve_delete_students.php")
    Call<Result> deletestudents(
            @Field("Action_type") String Action_type,
            @Field("ID") String ID
    );

    @FormUrlEncoded
    @POST("approve_delete_companies.php")
    Call<Result> approvecompanies(
            @Field("Action_type") String Action_type,
            @Field("ID") String ID
    );
    @FormUrlEncoded
    @POST("approve_delete_companies.php")
    Call<Result> deletecompanies(
            @Field("Action_type") String Action_type,
            @Field("ID") String ID
    );

//
    @FormUrlEncoded
    @POST("getcompanyapplications.php")
    Call<CompanyApplications> getjoblists(
            @Field("string") String string
    );
    @FormUrlEncoded
    @POST("getcollegeapplications.php")
    Call<CollegeApplications> getcollegejoblists(
            @Field("string") String string
    );

    @FormUrlEncoded
    @POST("collegerequest.php")
    Call<Result> collegemakereq(
            @Field("comname") String comname,
            @Field("place_interview") String place_interview,
            @Field("requested_date") String requested_date,
            @Field("interview_date") String interview_date,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("language") String language,
            @Field("website") String website,
            @Field("googleformlink") String googleformlink,
            @Field("other_details") String other_details
    );

//    @Multipart
//    @POST("studentApplication.php")
//    Call<Result> studentapplication(
//            @Part("studID") RequestBody studID,
//            @Part("cr_id") RequestBody cr_id,
//            @Part("comname") RequestBody comname,
//            @Part("language") RequestBody language,
//            @Part("interview_date") RequestBody interview_date,
//            @Part("email") RequestBody email,
//            @Part("mobile") RequestBody mobile,
//            @Part("interview_place") RequestBody interview_place,
//            @Part("website") RequestBody website,
//            @Part("otherdetails") RequestBody otherdetails,
//            @Part("cgpa") RequestBody cgpa,
//            @Part("applied_date") RequestBody applied_date,
//            @Part MultipartBody.Part photobody,
//            @Part MultipartBody.Part filebody
//    );

    @FormUrlEncoded
    @POST("getstudentapplication.php")
    Call<StudentApplications> getstudentapplications(
            @Field("user_type") String user_type,
            @Field("cr_id") String cr_id
    );

}
