package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CollegeApplications {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("college_requests_details")
    private ArrayList<CollegeApplicationsDetails> requests_details;

    public CollegeApplications(boolean success, String msg, ArrayList<CollegeApplicationsDetails> requests_details) {
        this.success = success;
        this.msg = msg;
        this.requests_details = requests_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<CollegeApplicationsDetails> getRequests_details() {
        return requests_details;
    }
}
