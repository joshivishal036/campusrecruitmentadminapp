package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

public class Studentdetail {

    @SerializedName("student_id")
    private String student_id;

    @SerializedName("student_profileimage_url")
    private String student_profileimage_url;

    @SerializedName("student_name")
    private String student_name;

    @SerializedName("student_gender")
    private String student_gender;

    @SerializedName("student_email")
    private String student_email;

    @SerializedName("student_mobile")
    private String student_mobile;

    @SerializedName("student_address")
    private String student_address;

    @SerializedName("student_state")
    private String student_state;

    @SerializedName("student_city")
    private String student_city;

    @SerializedName("student_collegename")
    private String student_collegename;

    @SerializedName("student_branch")
    private String student_branch;

    @SerializedName("student_semester")
    private String student_semester;

    @SerializedName("student_enrollment")
    private String student_enrollment;

    @SerializedName("student_status")
    private String student_status;

    public Studentdetail(String student_id,String student_profileimage_url, String student_name, String student_gender, String student_email, String student_mobile, String student_address, String student_state, String student_city, String student_collegename, String student_branch, String student_semester, String student_enrollment,String student_status) {
        this.student_id = student_id;
        this.student_profileimage_url = student_profileimage_url;
        this.student_name = student_name;
        this.student_gender = student_gender;
        this.student_email = student_email;
        this.student_mobile = student_mobile;
        this.student_address = student_address;
        this.student_state = student_state;
        this.student_city = student_city;
        this.student_collegename = student_collegename;
        this.student_branch = student_branch;
        this.student_semester = student_semester;
        this.student_enrollment = student_enrollment;
        this.student_status = student_status;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getStudent_profileimage_url() {
        return student_profileimage_url;
    }

    public String getStudent_name() {
        return student_name;
    }

    public String getStudent_gender() {
        return student_gender;
    }

    public String getStudent_email() {
        return student_email;
    }

    public String getStudent_mobile() {
        return student_mobile;
    }

    public String getStudent_address() {
        return student_address;
    }

    public String getStudent_state() {
        return student_state;
    }

    public String getStudent_city() {
        return student_city;
    }

    public String getStudent_collegename() {
        return student_collegename;
    }

    public String getStudent_branch() {
        return student_branch;
    }

    public String getStudent_semester() {
        return student_semester;
    }

    public String getStudent_enrollment() {
        return student_enrollment;
    }

    public String getStudent_status() {
        return student_status;
    }
}
