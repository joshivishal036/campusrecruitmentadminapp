package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

public class CompanyApplicationsDetails {

    @SerializedName("cr_id")
    private String cr_id;

    @SerializedName("c_id")
    private String c_id;

    @SerializedName("cr_company_name")
    private String cr_company_name;

    @SerializedName("cr_interview_place")
    private String cr_interview_place;

    @SerializedName("cr_requested_date")
    private String cr_requested_date;

    @SerializedName("cr_interview_date")
    private String cr_interview_date;

    @SerializedName("cr_email")
    private String cr_email;

    @SerializedName("cr_mobile")
    private String cr_mobile;

    @SerializedName("cr_language")
    private String cr_language;

    @SerializedName("cr_website")
    private String cr_website;

    @SerializedName("cr_otherdetails")
    private String cr_otherdetails;

    public CompanyApplicationsDetails(String cr_id, String c_id, String cr_company_name, String cr_interview_place, String cr_requested_date, String cr_interview_date, String cr_email, String cr_mobile, String cr_language, String cr_website, String cr_otherdetails) {
        this.cr_id = cr_id;
        this.c_id = c_id;
        this.cr_company_name = cr_company_name;
        this.cr_interview_place = cr_interview_place;
        this.cr_requested_date = cr_requested_date;
        this.cr_interview_date = cr_interview_date;
        this.cr_email = cr_email;
        this.cr_mobile = cr_mobile;
        this.cr_language = cr_language;
        this.cr_website = cr_website;
        this.cr_otherdetails = cr_otherdetails;
    }

    public String getCr_id() {
        return cr_id;
    }

    public String getC_id() {
        return c_id;
    }

    public String getCr_company_name() {
        return cr_company_name;
    }

    public String getCr_interview_place() {
        return cr_interview_place;
    }

    public String getCr_requested_date() {
        return cr_requested_date;
    }

    public String getCr_interview_date() {
        return cr_interview_date;
    }

    public String getCr_email() {
        return cr_email;
    }

    public String getCr_mobile() {
        return cr_mobile;
    }

    public String getCr_language() {
        return cr_language;
    }

    public String getCr_website() {
        return cr_website;
    }

    public String getCr_otherdetails() {
        return cr_otherdetails;
    }
}
