package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Companies {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("new_company_details")
    private ArrayList<DetailsCompany> new_company_details;

    @SerializedName("approved_company_details")
    private ArrayList<DetailsCompany> approved_company_details;

    public Companies(boolean success, String msg, ArrayList<DetailsCompany> new_company_details, ArrayList<DetailsCompany> approved_company_details) {
        this.success = success;
        this.msg = msg;
        this.new_company_details = new_company_details;
        this.approved_company_details = approved_company_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<DetailsCompany> getNew_company_details() {
        return new_company_details;
    }

    public ArrayList<DetailsCompany> getApproved_company_details() {
        return approved_company_details;
    }
}
