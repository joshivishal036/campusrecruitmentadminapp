package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StudentApplications {
    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("applications")
    ArrayList<StudentApplicationsDetails> studentApplicationsDetails;

    public StudentApplications(boolean success,String msg, ArrayList<StudentApplicationsDetails> studentApplicationsDetails) {
        this.success = success;
        this.msg = msg;
        this.studentApplicationsDetails = studentApplicationsDetails;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<StudentApplicationsDetails> getStudentApplicationsDetails() {
        return studentApplicationsDetails;
    }
}
