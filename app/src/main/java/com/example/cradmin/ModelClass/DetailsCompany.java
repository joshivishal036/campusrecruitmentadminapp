package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

public class DetailsCompany {

    @SerializedName("company_id")
    private String company_id;

    @SerializedName("company_name")
    private String company_name;

    @SerializedName("company_profileimage_url")
    private String company_profileimage_url;

    @SerializedName("company_email")
    private String company_email;

    @SerializedName("company_mobile")
    private String company_mobile;

    @SerializedName("company_address")
    private String company_address;

    @SerializedName("company_state")
    private String company_state;

    @SerializedName("company_city")
    private String company_city;

    @SerializedName("company_website")
    private String company_website;

    @SerializedName("company_status")
    private String company_status;

    public DetailsCompany(String company_id,String company_profileimage_url, String company_name, String company_email, String company_mobile, String company_address, String company_state, String company_city,String company_website,String company_status) {
        this.company_id = company_id;
        this.company_profileimage_url = company_profileimage_url;
        this.company_name = company_name;
        this.company_email = company_email;
        this.company_mobile = company_mobile;
        this.company_address = company_address;
        this.company_state = company_state;
        this.company_city = company_city;
        this.company_website = company_website;
        this.company_status=company_status;
    }

    public String getCompany_id() {
        return company_id;
    }

    public String getCompany_profileimage_url() {
        return company_profileimage_url;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getCompany_email() {
        return company_email;
    }

    public String getCompany_mobile() {
        return company_mobile;
    }

    public String getCompany_address() {
        return company_address;
    }

    public String getCompany_state() {
        return company_state;
    }

    public String getCompany_city() {
        return company_city;
    }

    public String getCompany_website() {
        return company_website;
    }

    public String getCompany_status() {
        return company_status;
    }
}
