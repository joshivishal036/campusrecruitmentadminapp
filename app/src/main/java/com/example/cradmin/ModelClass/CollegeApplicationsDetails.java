package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

public class CollegeApplicationsDetails {

    @SerializedName("cor_id")
    private String cor_id;

    @SerializedName("cor_company_name")
    private String cor_company_name;

    @SerializedName("cor_interview_place")
    private String cor_interview_place;

    @SerializedName("cor_requested_date")
    private String cor_requested_date;

    @SerializedName("cor_interview_date")
    private String cor_interview_date;

    @SerializedName("cor_email")
    private String cor_email;

    @SerializedName("cor_mobile")
    private String cor_mobile;

    @SerializedName("cor_language")
    private String cor_language;

    @SerializedName("cor_website")
    private String cor_website;

    @SerializedName("cor_googleformlink")
    private String cor_googleformlink;

    @SerializedName("cor_otherdetails")
    private String cor_otherdetails;

    public CollegeApplicationsDetails(String cor_id, String cor_company_name, String cor_interview_place, String cor_requested_date, String cor_interview_date, String cor_email, String cor_mobile, String cor_language, String cor_website, String cor_googleformlink, String cor_otherdetails) {
        this.cor_id = cor_id;
        this.cor_company_name = cor_company_name;
        this.cor_interview_place = cor_interview_place;
        this.cor_requested_date = cor_requested_date;
        this.cor_interview_date = cor_interview_date;
        this.cor_email = cor_email;
        this.cor_mobile = cor_mobile;
        this.cor_language = cor_language;
        this.cor_website = cor_website;
        this.cor_googleformlink = cor_googleformlink;
        this.cor_otherdetails = cor_otherdetails;
    }

    public String getCor_id() {
        return cor_id;
    }

    public String getCor_company_name() {
        return cor_company_name;
    }

    public String getCor_interview_place() {
        return cor_interview_place;
    }

    public String getCor_requested_date() {
        return cor_requested_date;
    }

    public String getCor_interview_date() {
        return cor_interview_date;
    }

    public String getCor_email() {
        return cor_email;
    }

    public String getCor_mobile() {
        return cor_mobile;
    }

    public String getCor_language() {
        return cor_language;
    }

    public String getCor_website() {
        return cor_website;
    }

    public String getCor_googleformlink() {
        return cor_googleformlink;
    }

    public String getCor_otherdetails() {
        return cor_otherdetails;
    }
}
