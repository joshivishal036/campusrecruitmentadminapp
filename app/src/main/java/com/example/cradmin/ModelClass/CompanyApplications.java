package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompanyApplications {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("requests_details")
    private ArrayList<CompanyApplicationsDetails> requests_details;

    public CompanyApplications(boolean success, String msg, ArrayList<CompanyApplicationsDetails> requests_details) {
        this.success = success;
        this.msg = msg;
        this.requests_details = requests_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<CompanyApplicationsDetails> getRequests_details() {
        return requests_details;
    }
}
