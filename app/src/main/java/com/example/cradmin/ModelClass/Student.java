package com.example.cradmin.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Student {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("approved_student_details")
    private ArrayList<Studentdetail> approvedstudentdetails;

    @SerializedName("new_student_details")
    private ArrayList<Studentdetail> newstudentdetails;

    public Student(boolean success, String msg, ArrayList<Studentdetail> approvedstudentdetails,ArrayList<Studentdetail> newstudentdetails) {
        this.success = success;
        this.msg = msg;
        this.approvedstudentdetails = approvedstudentdetails;
        this.newstudentdetails = newstudentdetails;
    }

    public boolean GetSuccess() {
        return success;
    }

    public String GetMsg() {
        return msg;
    }

    public ArrayList<Studentdetail> getApprovedstudentdetails() {
        return approvedstudentdetails;
    }

    public ArrayList<Studentdetail> getNewstudentdetails() {
        return newstudentdetails;
    }
}
