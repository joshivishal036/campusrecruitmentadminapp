package com.example.cradmin;

import android.os.Bundle;

import com.example.cradmin.Adapters.CompaniesTabbedActivityAdapter;
import com.example.cradmin.Adapters.StudentTabbedActivityAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;



public class CompaniesTabbedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companies_tabbed);

        final ViewPager viewPager = findViewById(R.id.view_pager);

        TabLayout tabs = findViewById(R.id.tabs);
        tabs.addTab(tabs.newTab().setText("New"));
        tabs.addTab(tabs.newTab().setText("Approved"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        CompaniesTabbedActivityAdapter companiesTabbedActivityAdapter=new CompaniesTabbedActivityAdapter(getSupportFragmentManager(),tabs.getTabCount());
        viewPager.setAdapter(companiesTabbedActivityAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}