package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.Adapters.ApprovedStudentsRecyclerAdapter;
import com.example.cradmin.Adapters.NewStudentsRecyclerAdapter;
import com.example.cradmin.ModelClass.Student;
import com.example.cradmin.ModelClass.Studentdetail;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentApprovedStudents extends Fragment implements ApprovedStudentsRecyclerAdapter.Adapterclick {

    ArrayList<Studentdetail> details=new ArrayList<>();
    RecyclerView recyclerView;
    ApprovedStudentsRecyclerAdapter approvedStudentsRecyclerAdapter;

    public FragmentApprovedStudents() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_approved_students, container, false);

        recyclerView = root.findViewById(R.id.approved_students_recyclerview_widget);
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service=retrofit.create(ApiService.class);

        Call<Student> call=service.getstudents("jayshreeram");

        call.enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().GetSuccess())
                    {
                        details.clear();
                        details.addAll(response.body().getApprovedstudentdetails());
                        approvedStudentsRecyclerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(getContext(), response.body().GetMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext(), response.body().GetMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();

            }
        });
        approvedStudentsRecyclerAdapter =new ApprovedStudentsRecyclerAdapter(FragmentApprovedStudents.this,details);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(approvedStudentsRecyclerAdapter);
        approvedStudentsRecyclerAdapter.notifyDataSetChanged();


        return  root;
    }

    @Override
    public void recyclerviewclick(int position) {

        Intent intent=new Intent(getContext(),StudentDetailsActivity.class);
        intent.putExtra("ID",details.get(position).getStudent_id());
        intent.putExtra("profileimage", details.get(position).getStudent_profileimage_url());
        intent.putExtra("name", details.get(position).getStudent_name());
        intent.putExtra("gender", details.get(position).getStudent_gender());
        intent.putExtra("email", details.get(position).getStudent_email());
        intent.putExtra("mobile", details.get(position).getStudent_mobile());
        intent.putExtra("address", details.get(position).getStudent_address());
        intent.putExtra("state", details.get(position).getStudent_state());
        intent.putExtra("city", details.get(position).getStudent_city());
        intent.putExtra("college", details.get(position).getStudent_collegename());
        intent.putExtra("branch", details.get(position).getStudent_branch());
        intent.putExtra("semester", details.get(position). getStudent_semester());
        intent.putExtra("enroll", details.get(position).getStudent_enrollment());
        intent.putExtra("status", details.get(position).getStudent_status());
        startActivity(intent);

    }
}
