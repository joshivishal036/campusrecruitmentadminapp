package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.ModelClass.Result;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudentDetailsActivity extends AppCompatActivity {
    TextView studname_tv,gender_tv,semail_tv,smobile_tv,collegename_tv,branch_tv,sem_tv,enroll_num_tv,address_tv,state_tv,city_tv;
    CircleImageView imageView;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;
    Button approve_btn, delete_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView=findViewById(R.id.student_details_profile_image);
        studname_tv=findViewById(R.id.student_details_stud_name_tv);
        gender_tv=findViewById(R.id.student_details_gender_tv);
        semail_tv=findViewById(R.id.student_details_stud_email_tv);
        smobile_tv=findViewById(R.id.student_details_stud_mobile_tv);
        address_tv=findViewById(R.id.student_details_address_tv);
        state_tv=findViewById(R.id.student_details_state_tv);
        city_tv=findViewById(R.id.student_details_city_tv);
        collegename_tv=findViewById(R.id.student_details_college_tv);
        branch_tv=findViewById(R.id.student_details_branch_tv);
        sem_tv=findViewById(R.id.student_details_sem_tv);
        enroll_num_tv=findViewById(R.id.student_details_enrollment_tv);
        approve_btn=findViewById(R.id.approve_button);
        delete_btn=findViewById(R.id.delete_button);

        if (getIntent().getStringExtra("status").equals("Approve"))
        {
            approve_btn.setVisibility(View.GONE);
            delete_btn.setVisibility(View.GONE);
        }

//        Toast.makeText(this,getIntent().getStringExtra("stud_app_com_mobile") , Toast.LENGTH_SHORT).show();

        Glide.with(StudentDetailsActivity.this)
                .load(Api.BASE_URL+getIntent().getStringExtra("profileimage"))
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(imageView);

        ID=getIntent().getStringExtra("studID");
        Log.d("joshidetails", "iss....>>>"+getIntent().getStringExtra("studID"));
        studname_tv.setText(getIntent().getStringExtra("name"));
        Log.d("joshidetails", "iss....>>>"+getIntent().getStringExtra("profileimage"));
        gender_tv.setText(getIntent().getStringExtra("gender"));
        semail_tv.setText(getIntent().getStringExtra("email"));
        smobile_tv.setText(getIntent().getStringExtra("mobile"));
        address_tv.setText(getIntent().getStringExtra("address"));
        state_tv.setText(getIntent().getStringExtra("state"));
        city_tv.setText(getIntent().getStringExtra("city"));
        collegename_tv.setText(getIntent().getStringExtra("college"));
        branch_tv.setText(getIntent().getStringExtra("branch"));
        sem_tv.setText(getIntent().getStringExtra("semester"));
        enroll_num_tv.setText(getIntent().getStringExtra("enroll"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onclickApproved(View view) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor= new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient httpClient=new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiService=retrofit.create(ApiService.class);
        String Action_type="Approve";
        //Log.d("joshi", "iss....>>>"+Action_type+"..."+ID);
        Call<Result> call=apiService.approvestudents(Action_type,ID);


        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getSuccess())
                    {
                        Toast.makeText(StudentDetailsActivity.this, "Approved Successfully", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(StudentDetailsActivity.this,StudentTabbedActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(StudentDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(StudentDetailsActivity.this, "response.message()", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(StudentDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onclickDeleted(View view) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("please wait!!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient client=new OkHttpClient().newBuilder()
                .connectTimeout(360,TimeUnit.MINUTES)
                .readTimeout(360,TimeUnit.MINUTES)
                .writeTimeout(360,TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiService=retrofit.create(ApiService.class);
        String Action_type="Delete";
        Call<Result> call=apiService.deletestudents(Action_type, ID);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getSuccess())
                    {
                        Toast.makeText(StudentDetailsActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(StudentDetailsActivity.this,StudentTabbedActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(StudentDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(StudentDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(StudentDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
