package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.Adapters.StudentApplicationsRecyclerAdapter;
import com.example.cradmin.ModelClass.StudentApplications;
import com.example.cradmin.ModelClass.StudentApplicationsDetails;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudentApplicationsActivity extends AppCompatActivity implements StudentApplicationsRecyclerAdapter.ApplicationsAdapterclick

{
    ArrayList<StudentApplicationsDetails> details = new ArrayList<>();

    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    StudentApplicationsRecyclerAdapter studentApplicationsRecyclerAdapter;

    SharedPreferences sharedPreferences_for_login;
    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "admin", studID = "ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_applications);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.studentapplicationrecyclerview);

//        setdatatoadapter(user_type, sharedPreferences_for_login.getString(studID, ""));

        progressDialog = new ProgressDialog(StudentApplicationsActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        Call<StudentApplications> call = apiService.getstudentapplications(user_type,"ssx");

        call.enqueue(new Callback<StudentApplications>() {
            @Override
            public void onResponse(Call<StudentApplications> call, Response<StudentApplications> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        details.clear();
                        details.addAll(response.body().getStudentApplicationsDetails());
                        studentApplicationsRecyclerAdapter = new StudentApplicationsRecyclerAdapter(StudentApplicationsActivity.this, details);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(StudentApplicationsActivity.this));
                        recyclerView.setAdapter(studentApplicationsRecyclerAdapter);
                        studentApplicationsRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(StudentApplicationsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(StudentApplicationsActivity.this, "null body", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StudentApplications> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(StudentApplicationsActivity.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public void recyclerviewclick(int position) {
//        Toast.makeText(this, "position is" + position, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(StudentApplicationsActivity.this, StudentApplicationDetailsActivity.class);
        intent.putExtra("stud_app_id",details.get(position).getStud_app_id());
        intent.putExtra("stud_app_stud_id",details.get(position).getStud_app_stud_id());
        intent.putExtra("stud_app_cr_id",details.get(position).getStud_app_cr_id());
        intent.putExtra("stud_app_stud_profileimage_url",details.get(position).getStud_app_stud_profileimage_url());
        intent.putExtra("stud_app_stud_name",details.get(position).getStud_app_stud_name());
        intent.putExtra("stud_app_stud_gender",details.get(position).getStud_app_stud_gender());
        intent.putExtra("stud_app_stud_email",details.get(position).getStud_app_stud_email());
        intent.putExtra("stud_app_stud_mobile",details.get(position).getStud_app_stud_mobile());
        intent.putExtra("stud_app_stud_state",details.get(position).getStud_app_stud_state());
        intent.putExtra("stud_app_stud_city",details.get(position).getStud_app_stud_city());
        intent.putExtra("stud_app_stud_clgname",details.get(position).getStud_app_stud_clgname());
        intent.putExtra("stud_app_stud_branch",details.get(position).getStud_app_stud_branch());
        intent.putExtra("stud_app_stud_sem",details.get(position).getStud_app_stud_sem());
        intent.putExtra("stud_app_stud_enrollment",details.get(position).getStud_app_stud_enrollment());
        intent.putExtra("stud_app_stud_cgpa",details.get(position).getStud_app_stud_cgpa());
        intent.putExtra("stud_app_stud_applieddate",details.get(position).getStud_app_stud_applieddate());
        intent.putExtra("stud_app_stud_photourl",details.get(position).getStud_app_stud_photourl());
        intent.putExtra("stud_app_stud_fileurl",details.get(position).getStud_app_stud_fileurl());
        intent.putExtra("stud_app_com_name",details.get(position).getStud_app_com_name());
        intent.putExtra("stud_app_com_language",details.get(position).getStud_app_com_language());
        intent.putExtra("stud_app_com_interdate",details.get(position).getStud_app_com_interdate());
        intent.putExtra("stud_app_com_email",details.get(position).getStud_app_com_email());
        intent.putExtra("stud_app_com_mobile",details.get(position).getStud_app_com_mobile());
        intent.putExtra("stud_app_com_interplace",details.get(position).getStud_app_com_interplace());
        intent.putExtra("stud_app_com_website",details.get(position).getStud_app_com_website());
        intent.putExtra("stud_app_com_otherdetails",details.get(position).getStud_app_com_otherdetails());
        startActivity(intent);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
