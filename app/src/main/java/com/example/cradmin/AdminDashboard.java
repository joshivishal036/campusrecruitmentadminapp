package com.example.cradmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.cradmin.Adapters.AdminDashRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class AdminDashboard extends AppCompatActivity implements AdminDashRecyclerAdapter.Adapterclick {

    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;

    RecyclerView dashboard_recyclerview;
    ArrayList<String> item_name=new ArrayList<>();
    ArrayList<Integer> icon_image=new ArrayList<>();
    AdminDashRecyclerAdapter adminDashRecyclerAdapter=new AdminDashRecyclerAdapter(AdminDashboard.this,icon_image,item_name);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dashboard_recyclerview=findViewById(R.id.admin_dashboard_recyclerview);
        item_name.add("Students");
        item_name.add("Companies");
        item_name.add("Student Applications");
        item_name.add("Comapny Applications");
        item_name.add("Make JobRequests");
        item_name.add("Contact us");
        icon_image.add(R.drawable.applications);
        icon_image.add(R.drawable.stud_dash_companies_icon);
        icon_image.add(R.drawable.form);
        icon_image.add(R.drawable.registration_icon);
        icon_image.add(R.drawable.collegesidejobreq);
        icon_image.add(R.drawable.stud_dash_contactus_icon);

        dashboard_recyclerview.setHasFixedSize(true);
        dashboard_recyclerview.setLayoutManager(new GridLayoutManager(this, 2));
        dashboard_recyclerview.setAdapter(adminDashRecyclerAdapter);

    }

    @Override
    public void recyclerviewclick(int position) {
        switch (position){
            case 0:
                Intent intent2=new Intent(AdminDashboard.this, StudentTabbedActivity.class);
                startActivity(intent2);
                break;
            case 1:
                Intent intent1=new Intent(AdminDashboard.this, CompaniesTabbedActivity.class);
                startActivity(intent1);
                break;
            case 2:
                Intent intent0=new Intent(AdminDashboard.this, StudentApplicationsActivity.class);
                startActivity(intent0);
                break;
            case 3:
                Intent intent4=new Intent(AdminDashboard.this, CompanyApplicationTabbedActivity.class);
                startActivity(intent4);
                break;
            case 4:
                Intent intent5=new Intent(AdminDashboard.this, JobApplication.class);
                startActivity(intent5);
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}

