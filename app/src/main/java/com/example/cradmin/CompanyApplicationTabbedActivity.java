package com.example.cradmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cradmin.Adapters.ComApplicationsTabbedActivityAdapter;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;


public class CompanyApplicationTabbedActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_application_tabbed);

        final ViewPager viewPager = findViewById(R.id.view_pager);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.addTab(tabs.newTab().setText("Company"));
        tabs.addTab(tabs.newTab().setText("College"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        ComApplicationsTabbedActivityAdapter comApplicationsTabbedActivityAdapter=new ComApplicationsTabbedActivityAdapter(getSupportFragmentManager(),tabs.getTabCount());
        viewPager.setAdapter(comApplicationsTabbedActivityAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
        }
        return super.onOptionsItemSelected(item);
    }
}