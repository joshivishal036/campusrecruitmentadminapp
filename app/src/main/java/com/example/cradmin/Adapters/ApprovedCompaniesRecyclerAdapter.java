package com.example.cradmin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.cradmin.APIs.Api;
import com.example.cradmin.FragmentApprovedCompanies;
import com.example.cradmin.FragmentNewCompanies;
import com.example.cradmin.ModelClass.DetailsCompany;
import com.example.cradmin.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ApprovedCompaniesRecyclerAdapter extends RecyclerView.Adapter<ApprovedCompaniesRecyclerAdapter.ViewHolder> {
//    ArrayList<Integer> icon_image;
    ArrayList<DetailsCompany> details;
    FragmentApprovedCompanies fragmentApprovedCompanies;
    Adapterclick adapterclick;
//    Student_Dashboard student_dashboard;


    public ApprovedCompaniesRecyclerAdapter(FragmentApprovedCompanies fragmentApprovedCompanies, ArrayList<DetailsCompany> details) {
        this.fragmentApprovedCompanies=fragmentApprovedCompanies;
//        this.icon_image = icon_image;
        this.details = details;

        try
        {
            this.adapterclick=((Adapterclick)fragmentApprovedCompanies);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.admin_companies_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.companyname_tv.setText(details.get(position).getCompany_name());
        holder.mail_tv.setText(details.get(position).getCompany_email());
        holder.city_tv.setText(details.get(position).getCompany_city());
        Glide.with(fragmentApprovedCompanies)
                .load(Api.BASE_URL+details.get(position).getCompany_profileimage_url())
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(holder.company_profile_imageview);

//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyname_tv;
        public TextView mail_tv;
        public TextView city_tv;
        public CircleImageView company_profile_imageview;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);
//            this.imageView=list.findViewById(R.id.stud_dash_recyclerlist_icon_image);
            this.companyname_tv=list.findViewById(R.id.view_company_recyclerlist_companyname_tv);
            this.mail_tv=list.findViewById(R.id.view_company_recyclerlist_mail_tv);
            this.city_tv=list.findViewById(R.id.view_company_recyclerlist_city_tv);
            this.company_profile_imageview=list.findViewById(R.id.view_companies_profile_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface Adapterclick
    {
        public void recyclerviewclick(int position);
    }
}

