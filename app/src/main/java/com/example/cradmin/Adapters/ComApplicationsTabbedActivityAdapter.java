package com.example.cradmin.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.cradmin.FragmentCompanyAppliationsCom;
import com.example.cradmin.FragmentCompanyApplicationsColl;

public class ComApplicationsTabbedActivityAdapter extends FragmentPagerAdapter {
    Integer tabcount;
    public ComApplicationsTabbedActivityAdapter(@NonNull FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount=tabcount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                FragmentCompanyAppliationsCom fragmentCompanyAppliationsCom=new FragmentCompanyAppliationsCom();
                return fragmentCompanyAppliationsCom;

            case 1:
               FragmentCompanyApplicationsColl fragmentCompanyApplicationsColl=new FragmentCompanyApplicationsColl();
               return fragmentCompanyApplicationsColl;
            default:
                return  null;

        }
//        return  null;
    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
