package com.example.cradmin.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.cradmin.FragmentCompanyAppliationsCom;
import com.example.cradmin.ModelClass.CompanyApplicationsDetails;
import com.example.cradmin.R;

import java.util.ArrayList;

public class CompanyApplicationsRecyclerAdapter extends RecyclerView.Adapter<CompanyApplicationsRecyclerAdapter.ViewHolder> {

    ArrayList<CompanyApplicationsDetails> details;
    FragmentCompanyAppliationsCom fragmentCompanyAppliationsCom;

    Adapterclick adapterclick;


    public CompanyApplicationsRecyclerAdapter(FragmentCompanyAppliationsCom fragmentCompanyAppliationsCom, ArrayList<CompanyApplicationsDetails> details) {
        this.fragmentCompanyAppliationsCom=fragmentCompanyAppliationsCom;

        this.details = details;

        try
        {
            this.adapterclick=((Adapterclick)fragmentCompanyAppliationsCom);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.admin_company_applications_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.companyname_tv.setText(details.get(position).getCr_company_name());
        holder.language_tv.setText(details.get(position).getCr_language());
        holder.inter_date_tv.setText(details.get(position).getCr_interview_date());
        holder.place_tv.setText(details.get(position).getCr_interview_place());

//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyname_tv;
        public TextView language_tv;
        public TextView inter_date_tv;
        public TextView place_tv;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);
//            this.imageView=list.findViewById(R.id.stud_dash_recyclerlist_icon_image);
            this.companyname_tv=list.findViewById(R.id.company_applications_cname_tv);
            this.language_tv=list.findViewById(R.id.company_applications_language_tv);
            this.inter_date_tv=list.findViewById(R.id.company_applications_interviewdate_tv);
            this.place_tv=list.findViewById(R.id.company_applications_place_tv);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface Adapterclick
    {
        public void recyclerviewclick(int position);
    }
}

