package com.example.cradmin.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cradmin.AdminDashboard;
import com.example.cradmin.R;

import java.util.ArrayList;

public class AdminDashRecyclerAdapter extends RecyclerView.Adapter<AdminDashRecyclerAdapter.ViewHolder> {
    ArrayList<Integer> icon_image;
    ArrayList<String> item_name;
    Context context;
    Adapterclick adapterclick;
    AdminDashboard adminDashboard;


    public AdminDashRecyclerAdapter(AdminDashboard adminDashboard, ArrayList<Integer> icon_image, ArrayList<String> item_name) {
        this.context=adminDashboard;
        this.icon_image = icon_image;
        this.item_name = item_name;

        try
        {
            this.adapterclick=((Adapterclick)adminDashboard);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview=layoutInflater.inflate(R.layout.admin_dash_recyclerlist,parent,false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(item_name.get(position));
        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return item_name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);
            this.imageView=list.findViewById(R.id.stud_dash_recyclerlist_icon_image);
            this.textView=list.findViewById(R.id.stud_dash_recyclerlist_item_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }

    public interface Adapterclick
    {
        public void recyclerviewclick(int position);
    }
}

