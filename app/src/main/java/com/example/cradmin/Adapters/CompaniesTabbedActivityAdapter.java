package com.example.cradmin.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.cradmin.FragmentApprovedCompanies;
import com.example.cradmin.FragmentApprovedStudents;
import com.example.cradmin.FragmentNewCompanies;
import com.example.cradmin.FragmentNewStudent;

public class CompaniesTabbedActivityAdapter extends FragmentPagerAdapter {
    Integer tabcount;
    public CompaniesTabbedActivityAdapter(@NonNull FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount=tabcount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                FragmentNewCompanies fragmentNewCompanies=new FragmentNewCompanies();
                return  fragmentNewCompanies;

            case 1:
                FragmentApprovedCompanies fragmentApprovedCompanies=new FragmentApprovedCompanies();
                return fragmentApprovedCompanies;

            default:
                return  null;

        }
    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
