package com.example.cradmin.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.cradmin.FragmentApprovedStudents;
import com.example.cradmin.FragmentNewStudent;

public class StudentTabbedActivityAdapter extends FragmentPagerAdapter {
    Integer tabcount;
    public StudentTabbedActivityAdapter(@NonNull FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount=tabcount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                FragmentNewStudent fragmentNewStudent=new FragmentNewStudent();
                return fragmentNewStudent;

            case 1:
                FragmentApprovedStudents fragmentApprovedStudents=new FragmentApprovedStudents();
                return fragmentApprovedStudents;
            default:
                return  null;

        }
    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
