package com.example.cradmin.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.cradmin.ModelClass.StudentApplicationsDetails;
import com.example.cradmin.R;
import com.example.cradmin.StudentApplicationsActivity;

import java.util.ArrayList;

public class StudentApplicationsRecyclerAdapter extends RecyclerView.Adapter<StudentApplicationsRecyclerAdapter.ViewHolder> {
//    ArrayList<Integer> icon_image;
    ArrayList<StudentApplicationsDetails> details;
    Context context;
    ApplicationsAdapterclick adapterclick;
//    Student_Dashboard student_dashboard;


    public StudentApplicationsRecyclerAdapter(StudentApplicationsActivity studentApplicationsActivity, ArrayList<StudentApplicationsDetails> details) {
        this.context=studentApplicationsActivity;
//        this.icon_image = icon_image;
        this.details=details;

        try
        {
            this.adapterclick=((ApplicationsAdapterclick)studentApplicationsActivity);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.admin_stud_applications_recyclerlist,parent,false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.student_name_tv.setText(details.get(position).getStud_app_stud_name());
        holder.companyname_tv.setText(details.get(position).getStud_app_com_name());
        holder.language_tv.setText(details.get(position).getStud_app_com_language());
        holder.req_date_tv.setText(details.get(position).getStud_app_stud_applieddate());
//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView student_name_tv;
        public TextView companyname_tv;
        public TextView language_tv;
        public TextView req_date_tv;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);

            this.student_name_tv=list.findViewById(R.id.stud_app_sname_tv);
            this.companyname_tv=list.findViewById(R.id.stud_app_cname_tv);
            this.language_tv=list.findViewById(R.id.stud_app_language_tv);
            this.req_date_tv=list.findViewById(R.id.stud_app_requesteddate_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface ApplicationsAdapterclick
    {
        public void recyclerviewclick(int position);
    }
}

