package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.Adapters.CollegeApplicationsRecyclerAdapter;
import com.example.cradmin.ModelClass.CollegeApplications;
import com.example.cradmin.ModelClass.CollegeApplicationsDetails;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentCompanyApplicationsColl extends Fragment implements CollegeApplicationsRecyclerAdapter.Adapterclick {
    ProgressDialog progressDialog;
    ArrayList<CollegeApplicationsDetails> Details_array=new ArrayList<>();
    RecyclerView recyclerView;
    CollegeApplicationsRecyclerAdapter collegeApplicationsRecyclerAdapter;
    public FragmentCompanyApplicationsColl() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_company_applications_coll, container, false);


        recyclerView = root.findViewById(R.id.company_applications_col_recyclerview_widget);
        set_data();

        collegeApplicationsRecyclerAdapter = new CollegeApplicationsRecyclerAdapter(FragmentCompanyApplicationsColl.this, Details_array);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(collegeApplicationsRecyclerAdapter);
        collegeApplicationsRecyclerAdapter.notifyDataSetChanged();

        return root;
    }

    public void set_data() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<CollegeApplications> call = service.getcollegejoblists("hjvj");

        call.enqueue(new Callback<CollegeApplications>() {
            @Override
            public void onResponse(Call<CollegeApplications> call, Response<CollegeApplications> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        Details_array.clear();
                        Details_array.addAll(response.body().getRequests_details());
                        collegeApplicationsRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CollegeApplications> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void recyclerviewclick(int position) {

        Intent intent = new Intent(getContext(), CollegeApplicationDetailsActivity.class);
        intent.putExtra("cr_id", Details_array.get(position).getCor_id());
        intent.putExtra("companyname", Details_array.get(position).getCor_company_name());
        intent.putExtra("language", Details_array.get(position).getCor_language());
        intent.putExtra("interview_date", Details_array.get(position).getCor_interview_date());
        intent.putExtra("email", Details_array.get(position).getCor_email());
        intent.putExtra("mobile", Details_array.get(position).getCor_mobile());
        intent.putExtra("interview_place", Details_array.get(position).getCor_interview_place());
        intent.putExtra("website", Details_array.get(position).getCor_website());
        intent.putExtra("googleformlink", Details_array.get(position).getCor_googleformlink());
        intent.putExtra("otherdetails", Details_array.get(position).getCor_otherdetails());
        startActivity(intent);
}

}
