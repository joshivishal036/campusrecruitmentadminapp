package com.example.cradmin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.cradmin.APIs.Api;
import com.example.cradmin.APIs.ApiService;
import com.example.cradmin.ModelClass.Result;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;

public class CompaniesDetailsActivity extends AppCompatActivity {
    TextView name_tv,email_tv,mobile_tv,website_tv,location_tv;
    CircleImageView profile_imageview;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",adminID="adminID",adminpass="adminPassword",user_type="admin",ID,password;
    Button approve_btn,delete_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companies_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile_imageview=findViewById(R.id.companyDetails_profile_image);
        name_tv=findViewById(R.id.companyDetails_name);
        email_tv=findViewById(R.id.companyDetails_email);
        mobile_tv=findViewById(R.id.companyDetails_mobile);
        website_tv=findViewById(R.id.companyDetails_website);
        location_tv=findViewById(R.id.companyDetails_location);
        approve_btn=findViewById(R.id.approve_button);
        delete_btn=findViewById(R.id.delete_button);
        Log.d("joshistatus", "is...>>>>"+getIntent().getStringExtra("status"));
        Log.d("joshistatus", "is...>>>>"+getIntent().getStringExtra("comID"));
        ID=getIntent().getStringExtra("comID");
        if (getIntent().getStringExtra("status").equals("Approve"))
        {
            approve_btn.setVisibility(GONE);
            delete_btn.setVisibility(GONE);
        }

        Glide.with(CompaniesDetailsActivity.this)
                .load(Api.BASE_URL+getIntent().getStringExtra("profileimage"))
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(profile_imageview);
        name_tv.setText(getIntent().getStringExtra("name"));
        email_tv.setText(getIntent().getStringExtra("email"));
        mobile_tv.setText(getIntent().getStringExtra("mobile"));
        website_tv.setText(getIntent().getStringExtra("website"));
        location_tv.setText(getIntent().getStringExtra("address")+","+getIntent().getStringExtra("city")+","+getIntent().getStringExtra("state"));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onclickApproved(View view) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor= new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient httpClient=new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiService=retrofit.create(ApiService.class);
        String Action_type="Approve";
        //Log.d("joshi", "iss....>>>"+Action_type+"..."+ID);
        Call<Result> call=apiService.deletecompanies(Action_type,ID);


        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getSuccess())
                    {
                        Toast.makeText(CompaniesDetailsActivity.this, "Approved Successfully", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(CompaniesDetailsActivity.this,CompaniesTabbedActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(CompaniesDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(CompaniesDetailsActivity.this, "response.message()", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CompaniesDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onclickDeleted(View view) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("please wait!!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient client=new OkHttpClient().newBuilder()
                .connectTimeout(360,TimeUnit.MINUTES)
                .readTimeout(360,TimeUnit.MINUTES)
                .writeTimeout(360,TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiService=retrofit.create(ApiService.class);
        String Action_type="Delete";
        Call<Result> call=apiService.deletecompanies(Action_type, ID);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getSuccess())
                    {
                        Toast.makeText(CompaniesDetailsActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(CompaniesDetailsActivity.this,CompaniesTabbedActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(CompaniesDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(CompaniesDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CompaniesDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
